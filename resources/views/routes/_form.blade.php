<div class="form-group">
	<label for="route-from">Origem</label>
	<input type="text" name="from" value="{{ old('from', $route->from ?? null) }}" class="form-control" id="route-from">
	@if ($errors->has('from'))
	<span class="error-message">
		<strong>{{ $errors->first('from') }}</strong>
	</span>
	@endif
</div>

<div class="form-group">
	<label for="route-to">Destino</label>
	<input type="text" name="to" value="{{ old('to', $route->to ?? null) }}" class="form-control" id="route-to">
	@if ($errors->has('to'))
	<span class="error-message">
		<strong>{{ $errors->first('to') }}</strong>
	</span>
	@endif
</div>

<div class="form-group">
	<label for="route-start">Partida</label>
	<input type="text" name="start" value="{{ old('start', $route->start ?? null) }}" class="form-control" id="route-start">
	@if ($errors->has('start'))
	<span class="error-message">
		<strong>{{ $errors->first('start') }}</strong>
	</span>
	@endif
</div>

<div class="form-group">
	<label for="route-duration">Duração</label>
	<input type="text" name="duration" value="{{ old('duration', $route->duration ?? null) }}" class="form-control" id="route-duration">
	@if ($errors->has('duration'))
	<span class="error-message">
		<strong>{{ $errors->first('duration') }}</strong>
	</span>
	@endif
</div>

<div class="form-group">
	<label for="route-spots">Lugares</label>
	<input type="text" name="spots" value="{{ old('spots', $route->spots ?? null) }}" class="form-control" id="route-spots">
	@if ($errors->has('spots'))
	<span class="error-message">
		<strong>{{ $errors->first('spots') }}</strong>
	</span>
	@endif
</div>