@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<form action="{{ route('routes.store') }}" method="post">
				@csrf
				{{ method_field('post') }}
				@include('routes._form')
				<button type="submit" class="btn btn-primary">Criar</button>
			</form>
		</div>
	</div>
</div>
@endsection