@extends('layouts.app');

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">Data / Hora</th>
						<th scope="col">Origem</th>
						<th scope="col">Destino</th>
						<th scope="col">Lugares</th>
						<th scope="col">Duração</th>
						<th scope="col">#</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($routes as $route)
					<tr>
						<td>{{ $route->start }}</td>
						<td>{{ $route->from }}</td>
						<td>{{ $route->to }}</td>
						<td>{{ $route->spots }}</td>
						<td>{{ $route->duration }}</td>
						<td>
							<form action="{{ route('reserves.store') }}" method="post">
								@csrf
								{{ method_field('post') }}
								<input type="hidden" name="route-id" value="{{ $route->id }}">
								<button type="submit" class="btn btn-primary">Reservar</button>
							</form>
						</td>
						<tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		@endsection()