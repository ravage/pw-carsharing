@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<form action="{{ route('routes.update', $route->id) }}" method="post">
				@csrf
				{{ method_field('put') }}
				@include('routes._form')
				<button type="submit" class="btn btn-primary">Atualizar</button>
			</form>
		</div>
	</div>
</div>
@endsection