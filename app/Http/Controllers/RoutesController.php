<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Route;
use App\Http\Requests\RouteRequest;
use Illuminate\Support\Facades\Auth;

class RoutesController extends Controller
{
    public function __construct()
    {
        # $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = Route::all();
        $ctx = ['routes' => $routes];
        return view('routes.index', $ctx);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role != 'driver') {
            return abort(404);
        }

        return view('routes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RouteRequest $request)
    {
        $route = new Route($request->all());
        $route->user_id = Auth::id();
        $route->save();

        session()->flash('success', 'Guardado!');
        return redirect('/routes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role != 'driver') {
            return abort(404);
        }

        $route = Route::findOrFail($id);
        $ctx = ['route' => $route];
        return view('routes.edit', $ctx);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RouteRequest $request, $id)
    {
        $route = Route::findOrFail($id);
        $route->update($request->all());

        return redirect('/routes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
