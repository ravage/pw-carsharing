<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Route;
use App\Reserve;

class ReservesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $routeId = $request->input('route-id');
        $userId = Auth::id();

        $route = Route::where('start', '>', DB::raw("date('now')"))->findOrFail($routeId);
        $reserve = new Reserve();
        $reserve->user_id = $userId;
        $reserve->route_id = $routeId;
        $reserve->save();

        return redirect('/routes');
    }
}
