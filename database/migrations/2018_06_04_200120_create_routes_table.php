<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('routes', function ($t) {
			$t->increments('id');
			$t->string('from', 255);
			$t->string('to', 255);
			$t->timestamp('start');
			$t->integer('spots');
			$t->string('duration', 255);
			$t->integer('user_id')->index();
			$t->timestamps();
			
			$t->foreign('user_id')->references('users')->on('id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routes');
    }
}
